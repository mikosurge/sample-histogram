from django.core.validators import URLValidator
from rest_framework import serializers

from histogram.models import SearchHistory


val = URLValidator()


class SearchHistorySerializer(serializers.ModelSerializer):
    """Serializer for search history.
    """
    url = serializers.URLField()
    created_at = serializers.DateTimeField(
        format='%Y-%m-%d %H:%m',
        read_only=True
    )

    class Meta:
        model = SearchHistory
        fields = ('id', 'url', 'created_at')
        read_only_fields = ('id', 'created_at')
