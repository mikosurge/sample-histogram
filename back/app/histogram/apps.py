from django.apps import AppConfig


class HistogramConfig(AppConfig):
    name = 'histogram'
