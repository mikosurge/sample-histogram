from django.urls import path, include
from rest_framework.routers import DefaultRouter


from histogram import views


router = DefaultRouter()
router.register('histogram', views.SearchHistoryViewSet)


app_name = 'histogram'

urlpatterns = [
    path('', include(router.urls)),
]
