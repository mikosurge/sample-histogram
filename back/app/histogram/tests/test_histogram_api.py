from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token

from histogram.models import SearchHistory
from histogram.serializers import SearchHistorySerializer


HISTOGRAM_URL = '/api/word/histogram/'
LOGIN_URL = reverse('user:login')
testcreds = {
    'email': 'dummy@mysite.com',
    'password': 'longenough',
    'name': 'Dummy User',
}


class HistogramApiTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(**testcreds)
        self.client = APIClient()
        self.client.post(LOGIN_URL, testcreds)
        token = Token.objects.get(user__email=testcreds['email'])
        self.client.credentials(HTTP_AUTHORIZATION=f'token {token.key}')

    def test_submit_url(self):
        """Test the database recorded the submitted word if success.
        """
        self.client.post(HISTOGRAM_URL, {'url': 'https://www.google.com'})
        history = SearchHistory.objects.all()
        self.assertEqual(history.count(), 1)
        serializer = SearchHistorySerializer(history, many=True)
        res = self.client.get(HISTOGRAM_URL)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data, serializer.data)
        

    def test_submit_nonurl(self):
        """Test submitting wrong url results failed.
        """
        res = self.client.post(HISTOGRAM_URL, {'url': 'https://notexist'})
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        res = self.client.post(HISTOGRAM_URL, {'url': 'notexist'})
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)


