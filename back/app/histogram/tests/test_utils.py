from django.test import TestCase
from collections import Counter

from histogram.utils import (
    strip_apostrophe, inner, get_raw_text,
    extract_words, to_histogram
)

class UtilityTests(TestCase):
    def test_strip_apostrophe(self):
        """Test stripping apostrophe from a word.
        """
        chunk = 'dummy\'s'
        self.assertEqual(strip_apostrophe(chunk), 'dummy')

    def test_strip_inner(self):
        """Test getting internal part of a word.
        """
        chunk = '~!@user\'s%^&*'
        self.assertEqual(inner(chunk), 'user')

    def test_get_raw_text(self):
        """Test getting text content from html, ignoring all
        head and meta tags.
        """
        with open('testdata/sample.html', 'r') as f:
            html = f.read()
        with open('testdata/sample.txt', 'r') as f:
            correct_raw = f.read()
        self.assertEqual(get_raw_text(html), correct_raw)

    def test_extract_words(self):
        correct_words = [
            'this', 'should', 'be', 'fetched',
            'this', 'should', 'be',
            'this', 'should',
            'this'
        ]
        with open('testdata/sample.txt', 'r') as f:
            raw = f.read()
        self.assertEqual(list(extract_words(raw)), correct_words)

    def test_histogram(self):
        hist_counter = Counter(this=4, should=3, be=2, fetched=1)
        with open('testdata/sample.txt', 'r') as f:
            raw = f.read()
        self.assertEqual(
            to_histogram(extract_words(raw)), hist_counter.most_common()
        )
