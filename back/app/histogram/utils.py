import string
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
from bs4 import BeautifulSoup
from collections import Counter


SPECIAL_CHARS = ''.join(c for c in string.printable if not c.isalnum())


def strip_apostrophe(chunk):
    """Strip the apostrophe if the given word is possessive.
    """
    if len(chunk) > 2 and chunk[-2] == '\'':
        chunk = chunk[:-2]
    return chunk


def inner(chunk):
    """Extract the inner part of a string only, stripping all
    special characters and \'s if any.
    """
    return strip_apostrophe(chunk.strip(SPECIAL_CHARS))


def fetch(url):
    """Fetch HTML content from a given URL.
    """
    try:
        res = urlopen(url).read()
    except (ValueError, HTTPError, URLError) as e:
        raise
    else:
        return res


def get_raw_text(html):
    """Retrieve raw text from a given HTML content.
    """
    soup = BeautifulSoup(html, 'html.parser')
    for script in soup(['script', 'style', 'head', 'meta']):
        script.extract()
    return soup.get_text().strip()


def extract_words(text):
    """Extract words from a given raw text.
    Words surrounded by special characters also get extracted.
    """
    lines = (line.strip() for line in text.splitlines())
    chunks = (inner(chunk) for line in lines for chunk in line.split(' '))
    return (chunk.lower() for chunk in chunks if chunk.isalpha())


def to_histogram(words, n_most=None, to_dict=False):
    """Return the hash table that counts the occurence of words.
    """
    most_common_ = Counter(words).most_common(n_most)
    return dict(most_common_) if to_dict else most_common_

