from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from urllib.error import URLError, HTTPError


from histogram.models import SearchHistory
from histogram.utils import fetch, get_raw_text, extract_words, to_histogram
from histogram import serializers


class SearchHistoryViewSet(viewsets.ModelViewSet):
    queryset = SearchHistory.objects.all()
    serializer_class = serializers.SearchHistorySerializer
    http_method_names = ['options', 'get', 'post']
    authentication_classes = (TokenAuthentication, )

    def options(self, request, format=None):
        """
        Return a list of all users.
        """
        return Response({}, headers={
            'Access-Control-Allow-Headers': '*',
        })


    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_queryset()
        ).filter(user=self.request.user).order_by('-created_at')[:10]

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """Create a new search history.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        url = serializer.validated_data.get('url')
        try:
            html = fetch(url)
        except (HTTPError, URLError) as e:
            return Response(
                {'error': str(e.reason)},
                status=status.HTTP_400_BAD_REQUEST
            )
        histogram = to_histogram(
            extract_words(
                get_raw_text(html)
            ),
            n_most=100
        )
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({
            **serializer.data,
            'histogram': histogram
        }, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

