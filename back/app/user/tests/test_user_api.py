from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from rest_framework import status


REGISTER_URL = reverse('user:register')
LOGIN_URL = reverse('user:login')
AUTH_URL = reverse('user:auth')


def create_user(**params):
    return get_user_model().objects.create_user(**params)

testcreds = {
    'email': 'dummy@mysite.com',
    'password': 'longenough',
    'name': 'Dummy User',
}


class UserModelTests(TestCase):
    def test_create_user(self):
        """Test creating a new user is successful.
        """
        user = get_user_model().objects.create_user(
            email=testcreds['email'],
            password=testcreds['password']
        )
        self.assertEqual(user.email, testcreds['email'])
        self.assertTrue(user.check_password(testcreds['password']))

    def test_create_user_invalid_email(self):
        """Test creating a new user with invalid email raises error.
        """
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'longenough')

    def test_create_new_superuser(self):
        """Test creating a new superuser"""
        user = get_user_model().objects.create_superuser(
            'super@mysite.com',
            'longenough'
        )
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)


class PublibUserApiTests(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_register_success(self):
        """Test registering a valid user should be successful.
        """
        res = self.client.post(REGISTER_URL, testcreds)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        user = get_user_model().objects.get(**res.data)
        self.assertTrue(user.check_password(testcreds['password']))
        self.assertNotIn('password', res.data)

    def test_register_existing(self):
        """Test registering an existing user should be failed.
        """
        create_user(**testcreds)
        res = self.client.post(REGISTER_URL, testcreds)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_token(self):
        """Test that a token is returned upon registration.
        """
        create_user(**testcreds)
        res = self.client.post(LOGIN_URL, testcreds)
        self.assertIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_create_token_invalid_credentials(self):
        """Test that no token is returned if the given credential is invalid.
        """
        create_user(**testcreds)
        invalid_creds = {
            'email': 'dummy@mysite.com',
            'password': 'invalid'
        }
        res = self.client.post(LOGIN_URL, invalid_creds)
        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_token_missing_password(self):
        """Test that full credentials are required.
        """
        res = self.client.post(LOGIN_URL, {
            'email': 'dummy@mysite.com',
            'password': ''
        })
        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_authenticate_invalid_credentials(self):
        """Test that authenticating with no token returns unauthorized.
        """
        res = self.client.get(AUTH_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateUserApiTests(TestCase):
    def setUp(self):
        self.user = create_user(**testcreds)
        self.client = APIClient()
        self.client.post(LOGIN_URL, testcreds)
        token = Token.objects.get(user__email=testcreds['email'])
        self.client.credentials(HTTP_AUTHORIZATION=f'token {token.key}')

    def test_authenticate_with_token(self):
        """Test that using token to authenticate returns the user information.
        """
        res = self.client.get(AUTH_URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data.get('email'), testcreds['email'])
        self.assertEqual(res.data.get('name'), testcreds['name'])

    def test_authenticate_by_post_now_allowed(self):
        """Test that authentication using post is not allowed.
        """
        res = self.client.post(AUTH_URL, testcreds)
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

