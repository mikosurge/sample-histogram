from django.shortcuts import render
from rest_framework import generics, authentication, permissions, status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken, APIView
from rest_framework.settings import api_settings
from rest_framework.response import Response

from user.serializers import (
    UserSerializer, AuthTokenSerializer)


def index(request):
    return render(request, 'index.html')


class CreateUserView(generics.CreateAPIView):
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(APIView):
# class ManageUserView(generics.RetrieveUpdateAPIView):
    # serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication, )
    # permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, format=None):
        if request.user.is_anonymous:
            return Response({
                'detail': "Authentication credentials were not provided."
            }, status=status.HTTP_401_UNAUTHORIZED)
        return Response({
            'email': request.user.email,
            'name': request.user.name
        })
    # def get_object(self):
    #     return self.request.user
