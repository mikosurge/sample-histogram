# Word Histogram

The repository consists of two parts: ReactJS as the front-end framework, stored in `front`, and Django Rest Framework as back-end framework, stored in `back`, while the built source front-end has been already pushed into the back-end directory, stored in `back\build`.
The built front-end has its endpoint fixed to `127.0.0.1:8000` so that in case of changing endpoint, it needs to be re-built.

## Running the server
The quickest and simplest way to try out the repository is by starting the backend server directly. Consider doing these following steps:
```
<start your own Python virtual environment of 3.7 or greater>
cd ./back/app
python manage.py test
python manage.py migrate
python manage.py runserver
```
Upon starting the server, head to `127.0.0.1:8000` to test the app.

## Docker
The Docker attempt has not been tested thoroughly, due to my limited PC. This repository provides a `Dockerfile` and `docker-compose` to create and run the corresponding `container` of the server.
For the Docker approach:
```
<start your own Python virtual environment>
cd ./back/app
docker build .
docker-compose up
```

## Troubleshooting
### Different API endpoint
In case of using a different API endpoint, the front-end needs to be rebuilt. Make sure that NodeJS and npm are installed.
```
cd ./front
npm install
npm run build
cp  ./build ../back/app/build
```
### CORS
If CORS still strikes the project, consider uncomment those lines with `# added to solve CORS` and retry.

For more troubleshooting, contact me at spnether1411@gmail.com.

## Coverage
The backend API provides functionalities to:
- Create a simple User account.
- Calculate the histogram of a given URL.
- Save the last successful submissions of the User to quickly reuse.

