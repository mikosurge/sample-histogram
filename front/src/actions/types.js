// Categories
export const D_AUTH = 'AUTH';
export const D_HISTOGRAM = 'WORD';


// Authentication
export const SIGN_IN = `@@${D_AUTH}/SIGN_IN`;
export const SIGN_OUT = `@@${D_AUTH}/SIGN_OUT`;
export const FETCH_URLS = `@@${D_HISTOGRAM}/FETCH_HISTORY`;
export const SUBMIT_URL = `@@${D_HISTOGRAM}/SUBMIT_URL`;
