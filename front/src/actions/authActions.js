import local from '../apis/local';
import {
  SIGN_IN,
  SIGN_OUT,
} from './types';

import { getCookie } from '../shared/utils';


export const authGuard = () => {
  return async dispatch => {
    const token = localStorage.getItem("token");
    if (!token) {
      return dispatch({
        type: SIGN_OUT,
      });
    }

    local.get('/user/auth/',
      {
        headers: {
          'Authorization': `token ${token}`
        }
      })
      .then(
        response => {
          const user = response.data;
          return dispatch({
            type: SIGN_IN,
            payload: {
              user,
              token,
            }
          });
        }
      )
      .catch(
        err => {
          return dispatch({
            type: SIGN_OUT,
          });
        }
      );
  };
};


export const signUp = (formValues) => {
  return async dispatch => {
    const response = await local.post('user/register/', formValues);
    if (!response.data || !response.data.email) {
      return response;
    }

    const user = response.data;
    const tokenResp = await local.post('user/login/', {
      email: user.email,
      password: formValues.password
    });
    if (!tokenResp.data.token) {
      return tokenResp;
    }
    const { token } = tokenResp.data;
    localStorage.setItem("token", token);
    return dispatch({
      type: SIGN_IN,
      payload: {
        user,
        token
      }
    });
  };
};


export const signIn = (email, password) => {
  return async dispatch => {
    const response = await local.post('/user/login/', {
      email,
      password
    }, {
      headers: {
        'X-CSRFToken': getCookie('csrftoken')
      }
    });

    if (response.data) {
      const { token } = response.data;
      const user = {
        email: email,
        name: ''
      };
      localStorage.setItem("token", token);
      return dispatch({
        type: SIGN_IN,
        payload: {
          user,
          token,
        }
      });
    }
  };
};

export const signOut = () => {
  return async (dispatch, getState) => {

    localStorage.removeItem("token");
    return dispatch({
      type: SIGN_OUT,
      payload: {}
    });
  }
}
