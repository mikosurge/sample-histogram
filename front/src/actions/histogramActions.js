import local from '../apis/local';
import {
  FETCH_URLS,
  SUBMIT_URL,
} from './types';


export const fetchUrls = () => {
  return async (dispatch, getState) => {
    const { token } = getState().auth;
    const response = await local.get(
      'word/histogram/',
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `token ${token}`
        },
      }
    );
    if (response.data) {
      return dispatch({
        type: FETCH_URLS,
        payload: response.data,
      });
    }
  };
};

export const submitUrl = url => {
  return async (dispatch, getState) => {
    if (!url.startsWith('http://') && !url.startsWith('https://')) {
      url = `https://${url}`;
    }

    const { token } = getState().auth;
    let response;
    try {
      response = await local.post(
        'word/histogram/',
        {
          'url': url,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `token ${token}`
          },
        }
      );
    } catch (error) {
      await dispatch({
        type: SUBMIT_URL,
        payload: null,
      });
      throw error;
    }
    if (response.data) {
      return dispatch({
        type: SUBMIT_URL,
        payload: response.data,
      })
    }
  };
};
