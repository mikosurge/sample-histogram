import {
  SIGN_IN,
  SIGN_OUT,
} from '../actions/types';

const INITIAL_STATE = {
  isSignedIn: null,
  user: {
    name: null,
    email: null,
  },
  token: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SIGN_IN:
      if (state.isSignedIn) {
        return state;
      }

      const { user, token } = action.payload;
      return { ...state, isSignedIn: true, user, token };
    case SIGN_OUT:
      return { ...state, isSignedIn: false, user: { username: null, email: null }, token: null };
    default:
      return state;
  };
};
