import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import authReducer from './authReducers';
import histogramReducer from './histogramReducers';

export default combineReducers({
  auth: authReducer,
  form: formReducer,
  histogram: histogramReducer,
});
