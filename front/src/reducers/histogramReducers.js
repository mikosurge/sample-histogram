import {
  FETCH_URLS,
  SUBMIT_URL,
  SIGN_OUT,
} from '../actions/types';

const INITIAL_STATE = {
  history: [],
  result: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_URLS:
      return { ...state, history: action.payload };
    case SUBMIT_URL:
      if (!action.payload) {
        return {
          ...state,
          result: []
        };
      }
      const newHistory = state.history.slice(
        0, Math.max(state.history.length - 1, 0)
      );
      newHistory.unshift(action.payload);
      return {
        ...state,
        result: action.payload.histogram,
        history: newHistory,
      };
    case SIGN_OUT:
      return INITIAL_STATE;
    default:
      return state;
  }
};
