import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Box,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
} from '@material-ui/core';

import EnhancedTableHead from '../widget/table/EnhancedTableHead';


const headCells = [
  { id: 'no', label: 'No' },
  { id: 'word', label: 'Word' },
  { id: 'quantity', label: 'Quantity' },
];


const styles = theme => ({
  table: {
  },
  tableHead: {
    fontWeight: 700,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  paddingOption: {
    paddingLeft: theme.spacing(2),
  }
});


class HistogramTable extends React.Component {
  state = {
    isAscending: true,
    orderBy: 'quantity',
    page: 0,
    dense: true,
    rowsPerPage: 10,
  };

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  render() {
    const { classes, words } = this.props;
    const { page, dense, rowsPerPage } = this.state;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, words.length - page * rowsPerPage);

    return (
      <Box className={classes.tableWrapper}>
        <Table
          className={classes.table}
          aria-labelledby='table__title'
          size={dense ? 'small' : 'medium'}
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            classes={classes}
            headCells={headCells}
          />
          <TableBody>
            {words
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map(
                (row, index) => (
                  <TableRow
                    hover
                    tabIndex={-1}
                    key={row[0]}
                  >
                    <TableCell
                      id={`table__cell-${index}`}
                      component='th'
                      scope='row'
                    >
                      {index + 1}
                    </TableCell>
                    <TableCell align='left'>{row[0]}</TableCell>
                    <TableCell align='left'>{row[1]}</TableCell>
                  </TableRow>
                )
              )
            }
            {emptyRows > 0 && (
              <TableRow styles={{ height: 33 * emptyRows }}>
                <TableCell colSpan={headCells.length} />
              </TableRow>
            )}
          </TableBody>
        </Table>
        <TablePagination
          rowsPerPageOptions={[10]}
          component="div"
          count={words.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'previous page',
          }}
          nextIconButtonProps={{
            'aria-label': 'next page',
          }}
          onChangePage={this.handleChangePage}
        />
      </Box>
    );
  }
}

export default withStyles(styles)(HistogramTable);
