import React from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import { 
  Box,
  Button,
  Container,
  Grid,
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  Paper,
  Typography,
  LinearProgress,
} from '@material-ui/core';


import { fetchUrls, submitUrl } from '../../actions';
import NotificationBar from '../widget/NotificationBar';

import HistogramTable from './HistogramTable';
import HistogramHistoryTable from './HistogramHistoryTable';


const styles = theme => ({
  container: {
    height: '100%',
    padding: theme.spacing(1)
  },
  gridContainer: {
  },
  formControl: {
    margin: theme.spacing(1, 1, 2, 1),
    width: '90%',
  },
  paperCurrent: {
    padding: theme.spacing(2),
  },
  paperHistory: {
    padding: theme.spacing(2),
  },
  boxSearch: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formSearch: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
  },
  btnSearch: {
    fontWeight: 700,
    marginRight: theme.spacing(2),
  },
  divider: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  boxHistory: {
    textAlign: 'center',
    marginBottom: theme.spacing(1),
  },
  labelError: {
    textAlign: 'center',
    color: 'red',
  },
});


const anchorOrigin = {
  vertical: 'bottom',
  horizontal: 'right',
};


class HistogramView extends React.Component {
  state = {
    keyword: '',
    isSubmitting: false,
    openSnackbar: false,
    snackbarProps: {
      variant: 'error',
      message: '',
    },
  };

  componentDidMount() {
    this.props.fetchUrls();
  }

  onSearchChange = e => {
    this.setState({
      keyword: e.target.value,
    });
  };

  onSearchSubmit = e => {
    e.preventDefault();
    this.setState({ isSubmitting: true });
    this.props.submitUrl(this.state.keyword)
      .then(
        () => {
          this.triggerNotification({
            variant: 'success',
            message: "Done processing!",
          });
        }
      )
      .catch(
        error => {
          this.triggerNotification({
            variant: 'error',
            message: "Can not process the URL",
          });
        }
      )
      .finally(
        () => {
          this.setState({ isSubmitting: false });
        }
      );
  };
  
  handleSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ openSnackbar: false });
  };

  triggerNotification = ({ variant, message }) => {
    this.setState({
      openSnackbar: true,
      snackbarProps: {
        variant,
        message
      },
    });
  };

  handleOnRowClick = url => {
    this.setState({
      keyword: url,
    });
    this.setState({ isSubmitting: true });
    this.props.submitUrl(url)
      .then(
        () => {
          this.triggerNotification({
            variant: 'success',
            message: "Done processing!",
          });
        }
      )
      .catch(
        error => {
          this.triggerNotification({
            variant: 'error',
            message: "Can not process the URL",
          });
        }
      )
      .finally(
        () => {
          this.setState({ isSubmitting: false });
        }
      );
  };

  render() {
    const { classes, history, result } = this.props;
    const { keyword, isSubmitting } = this.state;

    return (
      <Container
        className={classes.container}
        maxWidth="xl"
        aria-describedby="progress__submitting"
        aria-busy={isSubmitting}
      >
        {isSubmitting &&
          <LinearProgress id="progress__submitting" />
        }
        <Grid
          className={classes.gridContainer}
          container
          spacing={1}
        >
          <Grid
            item
            xs={12}
            lg={8}
            md={8}
          >
            <Paper className={classes.paperCurrent}>
              <Box className={classes.boxSearch}>
                <form
                  onSubmit={this.onSearchSubmit}
                  className={classes.formSearch}
                >
                  <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="search__url">
                      Input URL
                    </InputLabel>
                    <Input
                      id="search__url"
                      aria-describedby="search__url__helper"
                      type="text"
                      value={keyword}
                      disabled={isSubmitting}
                      onChange={this.onSearchChange}
                    />
                    <FormHelperText id="search__url__helper">
                      Hit Enter or Submit to start
                    </FormHelperText>
                  </FormControl>
                </form>
                <Button
                  className={classes.btnSearch}
                  variant="contained"
                  color="primary"
                  onClick={this.onSearchSubmit}
                  disabled={!this.state.keyword}
                >
                  Submit
                </Button>
              </Box>
              <Box className={classes.boxResult}>
                <HistogramTable
                  words={result}
                />
              </Box>
            </Paper>
          </Grid>
          <Grid
            item
            xs={12}
            lg={4}
            md={4}
          >
            <Paper className={classes.paperHistory}>
              <Box className={classes.boxHistory}>
                <Typography variant='h6'>
                  History
                </Typography>
                <Typography variant='subtitle2'>
                  Click a row to submit again.
                </Typography>
              </Box>
              <HistogramHistoryTable
                history={history}
                onRowClick={this.handleOnRowClick}
              />
            </Paper>
          </Grid>
        </Grid>

        <NotificationBar
          variant={this.state.snackbarProps.variant}
          onClose={this.handleSnackbarClose}
          message={this.state.snackbarProps.message}
          anchorOrigin={anchorOrigin}
          open={this.state.openSnackbar}
          autoHideDuration={3000}
        />
      </Container>
    );
  }
}


const StyledHistogramView = withStyles(styles)(HistogramView);

const mapStateToProps = state => ({
  history: state.histogram.history,
  result: state.histogram.result
});

export default connect(mapStateToProps, {
  fetchUrls,
  submitUrl
})(StyledHistogramView);
