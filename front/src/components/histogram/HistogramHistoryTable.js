import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from '@material-ui/core';


import EnhancedTableHead from '../widget/table/EnhancedTableHead';


const headCells = [
  { id: 'time', label: 'Timestamp', width: '33%' },
  { id: 'url', label: 'URL', width: '67%' },
];


const styles = theme => ({
  table: {
    tableLayout: 'fixed',
    width: '100%',
  },
  tableHead: {
    fontWeight: 700,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  paddingOption: {
    paddingLeft: theme.spacing(2),
  },
  tableCell: {
    wordWrap: 'break-word',
    padding: theme.spacing(1),
  },
  rowHistory: {
    cursor: 'pointer',
  },
});


class HistoryTable extends React.Component {
  state = {
    isAscending: true,
    orderBy: 'quantity',
    page: 0,
    dense: true,
    rowsPerPage: 10,
  };

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  handleRowClick = url => e => {
    e.preventDefault();
    this.props.onRowClick(url);
  };

  render() {
    const { classes, history } = this.props;
    const { page, dense, rowsPerPage } = this.state;

    return (
      <Box className={classes.tableWrapper}>
        <Table
          className={classes.table}
          aria-labelledby='table__title'
          size={dense ? 'small' : 'medium'}
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            classes={classes}
            headCells={headCells}
          />
          <TableBody>
            {history
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map(
                (row, index) => (
                  <TableRow
                    className={classes.rowHistory}
                    hover
                    tabIndex={-1}
                    key={index}
                    onClick={this.handleRowClick(row.url)}
                  >
                    <TableCell
                      id={`table__cell-${index}`}
                      component='th'
                      scope='row'
                      className={classes.tableCell}
                    >
                      {row.created_at}
                    </TableCell>
                    <TableCell
                      className={classes.tableCell}
                      align='left'
                    >
                      {row.url}
                    </TableCell>
                  </TableRow>
                )
              )
            }
          </TableBody>
        </Table>
      </Box>
    );
  }
}

const StyledHistoryTable = withStyles(styles)(HistoryTable);

export default StyledHistoryTable;
