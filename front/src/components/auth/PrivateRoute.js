import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { connect } from 'react-redux';

import LoadingSpinner from '../widget/Loading';

const PrivateRoute = ({ component: Component, auth, title }) => {
  return (
    <Route
      render={
        props => (
          auth.isSignedIn
          ? <Component {...props} title={title || "Automation"} />
          : auth.isSignedIn === false
            ? <Redirect to={{ pathname: '/auth', state: { from: props.location } }} />
            : <LoadingSpinner />
        )
      }
    />
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, null, null, { pure: false })(PrivateRoute);