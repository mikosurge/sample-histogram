import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Card,
  FormControl,
  FormGroup,
  FormHelperText,
  FormLabel,
  Input,
  InputLabel,
} from '@material-ui/core';

import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { signUp } from '../../actions/authActions';
import history from '../../history';

const styles =  theme => ({
  cardContainer: {
    margin: 'auto',
    maxWidth: '36rem',
  },
  form: {
    margin: 'auto',
    padding: '1rem 3rem',
    maxWidth: '36rem',
  },
  formControl: {
    marginBottom: '1rem',
  },
  button: {
    margin: '0.25rem',
    fontFamily: "Lucida Sans Unicode, Lucida Grande, sans-serif",
  },
  buttonGroup: {
    textAlign: 'center',
  },
});

class SignUpForm extends React.Component {
  state = {
    title: 'Please fill up this form'
  };

  onSubmit = values => {
    this.props.signUp(values)
      .then(
        ret => history.push('/')
      )
      .catch(
        err => {
          const title = err.errmsg || 'Sign up failed, please try again';
          this.setState({
            title
          });
        }
      );
  };

  renderTextInput = ({
    input,
    label,
    type,
    required,
    inputId,
    helperText,
    meta: {
      touched, error
    },
  }) => {
    const { classes } = this.props;
    const req = required || false;
    const tp = type || "text";

    return (
      <FormControl
        className={classes.formControl}
        error={touched && !!error}
      >
        <InputLabel
          htmlFor={inputId}
          required={req}
        >
          {label}
        </InputLabel>
        <Input
          id={inputId}
          aria-describedby={`${inputId}__helper`}
          {...input}
          type={tp}
        />
        <FormHelperText
          id={`${inputId}__helper`}
        >
          {
            touched
            ? error || 'Looks good!'
            : `${helperText}`
          }
        </FormHelperText>
      </FormControl>
    );
  };

  renderBio = () => {
    return null;
  };

  render() {
    const { classes, handleSubmit, pristine, reset, submitting, invalid, anyTouched } = this.props;

    return (
      <Card
        className={classes.cardContainer}
      >
        <form
          onSubmit={handleSubmit(this.onSubmit)}
          className={classes.form}
        >
          <FormLabel
            component="legend"
            required
            error={anyTouched && invalid}
          >
            {this.state.title}
          </FormLabel>
          <FormGroup>
            <Field
              name="email"
              component={this.renderTextInput}
              label="Email"
              inputId="signup__email"
              helperText="Your email address"
              required
            />
            <Field
              name="name"
              component={this.renderTextInput}
              label="Name"
              inputId="signup__name"
              helperText="Your name"
              required
            />
            <Field
              name="password"
              component={this.renderTextInput}
              label="Password"
              type="password"
              inputId="signup__password"
              helperText="Your password"
              required
            />
            <Field
              name="rePassword"
              component={this.renderTextInput}
              label="Password Validatior"
              type="password"
              inputId="signup__re-password"
              helperText="Please re-enter your password"
              required
            />
          </FormGroup>
          <div className={classes.buttonGroup}>
            <Button
              disableRipple={true}
              variant="contained"
              color="primary"
              type="submit"
              disabled={invalid || submitting}
              className={classes.button}
            >
              Sign Up
            </Button>

            <Button
              disableRipple={true}
              variant="contained"
              color="secondary"
              disabled={pristine || submitting}
              onClick={reset}
              className={classes.button}
            >
              Clear Data
            </Button>
          </div>
        </form>
      </Card>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Required';
  } else if (values.name.length < 3) {
    errors.name = 'Your name is too short!';
  } else if (values.name.length > 20) {
    errors.name = 'Your name is too long!';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < 6) {
    errors.password = 'Your password is too short!';
  } else if (values.password.length > 20) {
    errors.password = 'Your password is too long!';
  }

  if (values.rePassword !== values.password) {
    errors.rePassword = 'These passwords are not matched.';
  }

  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(values.email)) {
    errors.email = 'Invalid email address.';
  }

  return errors;
};

const StyledSignUpForm = withStyles(styles)(SignUpForm);

const ReduxSignUpForm = reduxForm({
  form: 'SignUpForm',
  validate,
})(StyledSignUpForm);

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {
  signUp: signUp
})(ReduxSignUpForm);
