import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Card,
  FormControl,
  FormGroup,
  FormHelperText,
  FormLabel,
  Input,
  InputLabel,
} from '@material-ui/core';

import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { signIn } from '../../actions/authActions';
import history from '../../history';

import CustomLink from '../widget/CustomLink';

const styles = theme => ({
  authCard: {
    margin: 'auto',
    maxWidth: '30rem',
  },
  form: {
    margin: 'auto',
    padding: `1rem 3rem`,
    maxWidth: '30rem',
  },
  formControl: {
    marginBottom: '1rem',
  },
  buttonGroup: {
    textAlign: 'center',
  },
  button: {
    margin: '0.5rem',
  }
});

class AuthForm extends React.Component {
  state = {
    title: "Login"
  };

  onSubmit = values => {
    this.props.signIn(values.email, values.password)
      .then(
        () => {
          const { location } = this.props;
          if (location.state && location.state.from) {
            history.push(location.state.from.pathname);
          } else {
            history.push('/');
          }
        }
      )
      .catch(
        () => {
          this.props.reset();
          this.setState({
            title: 'Login failed, please retry!'
          });
        }
      );
  };

  renderInput = ({
    id,
    input,
    label,
    meta: {
      touched, error,
    },
    type,
    helperText,
  }) => {
    const { classes } = this.props;
    const inputId = `auth__${id}`;

    return (
      <FormControl
        className={classes.formControl}
        error={touched && !!error}
      >
        <InputLabel htmlFor={inputId}>
          {label}
        </InputLabel>
        <Input
          id={inputId}
          type={type}
          aria-describedby={`${inputId}__helper`}
          {...input}
        />
        <FormHelperText
          id={`${inputId}__helper`}
        >
          {
            touched
            ? error || 'Looks good!'
            : helperText
          }
        </FormHelperText>
      </FormControl>
    );
  };

  render() {
    const { classes, handleSubmit, submitting, invalid, anyTouched } = this.props;

    return (
      <Card
        className={classes.authCard}
      >
        <form
          onSubmit={handleSubmit(this.onSubmit)}
          className={classes.form}
        >
          <FormLabel
            component="legend"
            required
            error={anyTouched && invalid}
          >
            {this.state.title}
          </FormLabel>
          <FormGroup>
            <Field
              name="email"
              id="email"
              label="Email"
              component={this.renderInput}
              helperText="Your email"
            />
            <Field
              name="password"
              id="password"
              label="Password"
              type="password"
              component={this.renderInput}
              helperText="Your password"
            />
            <div className={classes.buttonGroup}>
              <Button
                className={classes.button}
                disableRipple={true}
                variant="contained"
                color="primary"
                type="submit"
                disabled={invalid || submitting}
              >
                Log In
              </Button>
              <Button
                className={classes.button}
                disableRipple={true}
                variant="contained"
                color="secondary"
                type="button"
                component={CustomLink}
                to="/signup"
              >
                Sign Up
              </Button>
            </div>
          </FormGroup>
        </form>
      </Card>
    );
  }
}

const validate = values => {
  const errrors = { };

  if (!values.email) {
    errrors.email = 'Required';
  }

  if (!values.email) {
    errrors.password = 'Required';
  }

  return errrors;
};

const StyledAuthForm = withStyles(styles)(AuthForm);

const ReduxAuthForm = reduxForm({
  form: 'AuthForm',
  validate,
})(StyledAuthForm);

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {
  signIn
})(ReduxAuthForm);
