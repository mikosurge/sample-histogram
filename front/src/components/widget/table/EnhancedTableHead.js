import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import {
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  tableCell: {
    backgroundColor: theme.palette.primary.main,
    color: 'white',
  },
}));

function EnhancedTableHead(props) {
  const {
    headCells,
  } = props;

  const defaultClasses = useStyles();

  return (
    <TableHead>
      <TableRow>
        {headCells.map(
          headCell => (
            <TableCell
              className={defaultClasses.tableCell}
              key={headCell.id}
              align={headCell.align || (headCell.numeric ? 'right' : 'left')}
              padding={headCell.disablePadding ? 'none' : 'default'}
              width={headCell.width}
            >
              {headCell.label}
            </TableCell>
          )
        )}
      </TableRow>
    </TableHead>
  )
};

export default EnhancedTableHead;
