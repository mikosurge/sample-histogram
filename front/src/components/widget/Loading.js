import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  CircularProgress,
  Typography,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  container: {
    height: '60vh',
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  spinner: {

  },
  text: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
}));

export default function LoadingSpinner() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <CircularProgress
        className={classes.spinner}
        size={144}
        thickness={3.6}
      />
      <Typography
        className={classes.text}
        align={"center"}
        color={"primary"}
        variant={"h6"}
      >
        Kindly wait when we fetch some data...
      </Typography>
    </div>
  );
};