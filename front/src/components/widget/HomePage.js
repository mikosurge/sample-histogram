import React from 'react';
import clsx from 'clsx';

import { withStyles } from '@material-ui/core/styles';

import { connect } from 'react-redux';
import {
  signIn,
  signOut,
} from '../../actions/authActions';

import history from '../../history';
import {
  AppBar,
  Button,
  Drawer,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@material-ui/core';

import AccountCircle from '@material-ui/icons/AccountCircle';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import InboxIcon from '@material-ui/icons/Inbox';
import MenuIcon from '@material-ui/icons/Menu';
import MoreIcon from '@material-ui/icons/More';

import HistogramView from '../histogram/HistogramView';

const drawerWidth = 240;

const styles = theme => ({
  container: {
    display: 'flex',
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(4),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    // width: theme.spacing(8),
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7),
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  mainContent: {
    flexGrow: 1,
    padding: theme.spacing(0),
  },
  contentButton: {

  },
  horizontalToolbar: {
    minHeight: `${theme.spacing(6)}px`,
    paddingLeft: `${theme.spacing(2)}px`,
    paddingRight: `${theme.spacing(2)}px`,
  },
  toolbar: {
    minHeight: `${theme.spacing(6)}px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: `0 ${theme.spacing(1)}px`,
  },
  labelUser: {
    margin: theme.spacing(0, 1),
  },
});


const menuId = "user-account-menu";
const mobileMenuId = "user-account-menu-mobile";

const anchorOrigin = {
  vertical: 'top',
  horizontal: 'right',
};

const transformOrigin = {
  vertical: 'top',
  horizontal: 'right',
}

class HomePage extends React.Component {
  state = {
    open: false,
    anchorElement: null,
    mobileAnchorElement: null,
    openEditProfile: false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
    // Workaround, following https://github.com/mui-org/material-ui/issues/9337
    setTimeout(() => {
      window.dispatchEvent(new CustomEvent('resize'));
    }, 50);
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
    // Workaround, following https://github.com/mui-org/material-ui/issues/9337
    setTimeout(() => {
      window.dispatchEvent(new CustomEvent('resize'));
    }, 50);
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorElement: event.currentTarget });
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileAnchorElement: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorElement: null });
    this.handleMobileMenuClose();
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileAnchorElement: null });
  };

  onLoginClick = () => {
    history.push("/auth");
  };

  onSignOut = () => {
    const { signOut } = this.props;
    signOut()
      .then(
        () => {
          history.push('/auth');
        }
      )
      .catch(
        err => {
          console.log(err);
        }
      );
  };

  renderMenu = () => {
    const { anchorElement } = this.state;
    const isMenuOpen = Boolean(anchorElement);

    return (
      <Menu
        anchorEl={anchorElement}
        anchorOrigin={anchorOrigin}
        id={menuId}
        keepMounted
        transformOrigin={transformOrigin}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <MenuItem
          onClick={() => {
            this.handleMenuClose();
            this.onSignOut();
          }}
        >
          Sign Out
        </MenuItem>
      </Menu>
    )
  };

  renderMobileMenu = () => {
    const { mobileAnchorElement } = this.state;
    const isMobileMenuOpen = Boolean(mobileAnchorElement);

    return (
      <Menu
        anchorEl={mobileAnchorElement}
        anchorOrigin={anchorOrigin}
        id={mobileMenuId}
        keepMounted
        transformOrigin={transformOrigin}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        <MenuItem onClick={this.handleProfileMenuOpen}>
          <IconButton
            aria-label="user profile"
            aria-controls="user-profile"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
    );
  };

  render() {
    const { auth, classes } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.container}>
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar
            className={classes.horizontalToolbar}
          >
            <IconButton
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
              color="inherit"
              aria-label="open drawer"
              onClick={this.handleDrawerOpen}
              edge="start"
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              noWrap
              className={classes.title}
            >
              Histogram
            </Typography>

            {!auth.isSignedIn &&
              <Button
                onClick={this.onLoginClick}
                color="inherit"
              >
                Login
              </Button>
            }

            {auth.isSignedIn &&
              <div className={classes.sectionDesktop}>
                <IconButton
                  edge="end"
                  aria-label="account of current user"
                  aria-controls={menuId}
                  aria-haspopup="true"
                  onClick={this.handleProfileMenuOpen}
                  color="inherit"
                >
                  <Typography
                    variant="subtitle2"
                    noWrap
                    className={classes.labelUser}
                  >
                    {auth.user.email}
                  </Typography>
                  <AccountCircle />
                </IconButton>
              </div>
            }

            {auth.isSignedIn &&
              <div className={classes.sectionMobile}>
                <IconButton
                  aria-label="show more"
                  aria-controls={mobileMenuId}
                  aria-haspopup="true"
                  onClick={this.handleMobileMenuOpen}
                  color="inherit"
                >
                  <MoreIcon />
                </IconButton>
              </div>
            }
          </Toolbar>
        </AppBar>
        {this.renderMobileMenu()}
        {this.renderMenu()}

        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
          open={open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>
            <ListItem
              button
            >
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary={"Histogram"} />
            </ListItem>
          </List>
        </Drawer>

        <main
          className={classes.mainContent}
        >
          <div className={classes.toolbar} />
          <HistogramView />
        </main>
      </div>
    );
  }
};

const mapStateToProps = state => ({
  auth: state.auth,
});

const StyledHomePage = withStyles(styles)(HomePage);

export default connect(mapStateToProps, {
  signIn,
  signOut,
})(StyledHomePage);
