import axios from 'axios';

import { SERVER_ENDPOINT } from '../shared/given';

const API_ENDPOINT = `${SERVER_ENDPOINT}/api`;

export default axios.create({
  baseURL: API_ENDPOINT
});
